
# Garbage detection using PyTorch and YoloV5
### (Work in progress)

For more information, look at [this](https://medium.com/maarten-sukel/garbage-object-detection-using-pytorch-and-yolov3-d6c4e0424a10) medium post.

PyTorch implementation of a garbage detection model. This repository contains all code for predicting/detecting and evaulating the model. The current version can detect garbage bags, cardboard and household waste containers.

This repository is based on https://github.com/ultralytics/yolov5

Test and prediction code for a garbage object detection

## Frame analyzer components

The Frame analyzer is a self-built PC, built with the following components:
* Case: Cooler Master Elite 130 (Mini-ITX case)
* Board: Asus Prime A320I-K/CSM (Mini-ITX motherboard, AM4 Socket)
* CPU: AMD Ryzen 7 2700X (AM4)
* CPU fan: Xilence XC040 (Note: space for a fan is limited due to case)
* RAM: Corsair Vengeance LPX CMK16GX4M1D3000 (1 x 16GB & 3.000MT/s, 2 RAM slots availible (32GB is max))
* GPU: Gigabyte GeForce RTX 2070 Super Gaming OC 3X 8G (analyzes one frame in under 0.07 seconds!)
* Storage: Kingston A400 240GB (for running OS)
* PSU: be quiet! Pure Power 11 500W CM (80 Plus Gold)

## Installation

To install all required libaries:
```
pip install -r requirements.txt
```

Download https://drive.google.com/drive/folders/1XjniIUKNIe-85DjkkHwDJmQ-U7N83BxY?usp=sharing and move weights to weights/garb_weights_l.pt

## Predictions

For an example how to make predictions see "Test MlWorker.py"

To run the model on an entire folder:

```
python detect.py --weights weights/garb_weights_l.pt --source inference/images
```

### Docker

To run code in docker
```
docker-compose build
docker-compose up
```
